import {Component, ViewChild} from '@angular/core';
import {NgForm} from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Forms';
  // @ts-ignore
  @ViewChild('f') signUpForm: NgForm;
  defaultQuestion = 'pet';
  answer = '';
  gender = ['male', 'female'];
  user = {
    name: '',
    email: '',
    question: '',
    answer: '',
    gender: ''
  };
  formSubmitted = false;
  /* onSubmit(f: NgForm) {
    console.log(f);
  }*/
  // on submit get current values of form entered by user
 onSubmit() {
   this.formSubmitted = true;
   this.user.name = this.signUpForm.value.userData.userName;
   this.user.email = this.signUpForm.value.userData.email;
   this.user.question = this.signUpForm.value.secret;
   this.user.answer = this.signUpForm.value.questionAnswer;
   this.user.gender = this.signUpForm.value.gen;
   // ------------------------------
   // to reset the form use reset()
   // it not only reset the form but also reset the classes value i.e
   // ng-valid to  ng-invalid, ng-touched to ng-untouched, etc
   this.signUpForm.reset();
 }

  suggestdata() {
    const name = 'superUser';
    /*setValue() -> to overwrite whole form*/
    /*this.signUpForm.setValue({
      userData: {
        userName: name,
        email: ''
      },
      secret: 'teacher',
      questionAnswer: '',
      gen: 'female'
    });*/
    /*patchValue -> to overwrite specific parts of form*/
    this.signUpForm.form.patchValue({
      userData: {
        userName: name,
       }
   });
  }
}
